unit RptLayout;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, System.IniFiles;

type
  TForm2 = class(TForm)
    btnNbr: TButton;
    btnName: TButton;
    btnHDim: TButton;
    btnVDim: TButton;
    btnSDim: TButton;
    btnLDim: TButton;
    btnArea: TButton;
    btnAUnit: TButton;
    BtnDUnit: TButton;
    btnPerim: TButton;
    btnPArea: TButton;
    btnPAUnit: TButton;
    btnVol: TButton;
    btnVUnits: TButton;
    btnPct: TButton;
    lblMsg: TLabel;
    Panel1: TPanel;
    ScrollBox1: TScrollBox;
    LblHeadings: TLabel;
    lblCol1: TLabel;
    lblCol2: TLabel;
    lblCol3: TLabel;
    lblCol4: TLabel;
    lblCol5: TLabel;
    lblCol6: TLabel;
    lblCol7: TLabel;
    lblCol8: TLabel;
    lblCol9: TLabel;
    lblCol10: TLabel;
    Shape1: TShape;
    edHead1: TEdit;
    edHead2: TEdit;
    edHead8: TEdit;
    edHead7: TEdit;
    edHead6: TEdit;
    edHead3: TEdit;
    edHead4: TEdit;
    edHead5: TEdit;
    edHead9: TEdit;
    edHead10: TEdit;
    edData1: TEdit;
    edData9: TEdit;
    edData8: TEdit;
    edData7: TEdit;
    edData6: TEdit;
    edData2: TEdit;
    edData3: TEdit;
    edData4: TEdit;
    edData5: TEdit;
    edData10: TEdit;
    LblData: TLabel;
    lblSubTots: TLabel;
    edSubTot1: TEdit;
    edSubTot2: TEdit;
    edSubTot3: TEdit;
    edSubTot4: TEdit;
    edSubTot5: TEdit;
    edSubTot6: TEdit;
    edSubTot7: TEdit;
    edSubTot8: TEdit;
    edSubTot9: TEdit;
    edSubTot10: TEdit;
    memInstructions: TMemo;
    Button1: TButton;
    Button2: TButton;
    LblTots: TLabel;
    EdTot1: TEdit;
    EdTot2: TEdit;
    EdTot3: TEdit;
    EdTot4: TEdit;
    EdTot5: TEdit;
    EdTot6: TEdit;
    EdTot7: TEdit;
    EdTot8: TEdit;
    EdTot9: TEdit;
    EdTot10: TEdit;
    BtnCat: TButton;
    BtnLyr: TButton;
    LblTitle: TLabel;
    EdTitle: TEdit;
    LblSubHeads: TLabel;
    EdSubHdg: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edExit(Sender: TObject);
    procedure btnNbrClick(Sender: TObject);
    procedure edEnter(Sender: TObject);
    procedure edHeadEnter(Sender: TObject);
    procedure btnNameClick(Sender: TObject);
    procedure btnHDimClick(Sender: TObject);
    procedure btnVDimClick(Sender: TObject);
    procedure btnSDimClick(Sender: TObject);
    procedure btnLDimClick(Sender: TObject);
    procedure btnPerimClick(Sender: TObject);
    procedure BtnDUnitClick(Sender: TObject);
    procedure btnAreaClick(Sender: TObject);
    procedure btnAUnitClick(Sender: TObject);
    procedure btnPAreaClick(Sender: TObject);
    procedure btnPAUnitClick(Sender: TObject);
    procedure btnVolClick(Sender: TObject);
    procedure btnVUnitsClick(Sender: TObject);
    procedure btnPctClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtnCatClick(Sender: TObject);
    procedure BtnLyrClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}
VAR
  Msgs : TStringList;
  LastEd : TEdit;
  LastCsr : integer;

procedure TForm2.btnAreaClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{area}', s, LastCsr+1);
    end
    else
      insert ('{area}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 6;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.btnAUnitClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{aunit}', s, LastCsr+1);
    end
    else
      insert ('{aunit}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 7;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.BtnCatClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{cat}', s, LastCsr+1);
    end
    else
      insert ('{cat}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 7;
    LastEd.SetFocus;
  end;
end;


procedure TForm2.BtnDUnitClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{dunit}', s, LastCsr+1);
    end
    else
      insert ('{dunit}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 7;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.btnHDimClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{hdim}', s, LastCsr+1);
    end
    else
      insert ('{hdim}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 6;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.btnLDimClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{ldim}', s, LastCsr+1);
    end
    else
      insert ('{ldim}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 6;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.BtnLyrClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{lyr}', s, LastCsr+1);
    end
    else
      insert ('{lyr}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 6;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.btnNameClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{name}', s, LastCsr+1);
    end
    else
      insert ('{name}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 6;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.btnNbrClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{nbr}', s, LastCsr+1);
    end
    else
      insert ('{nbr}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 5;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.btnPAreaClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{parea}', s, LastCsr+1);
    end
    else
      insert ('{parea}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 7;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.btnPAUnitClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{paunit}', s, LastCsr+1);
    end
    else
      insert ('{paunit}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 8;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.btnPctClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{area%}', s, LastCsr+1);
    end
    else
      insert ('{area%}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 7;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.btnPerimClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{perim}', s, LastCsr+1);
    end
    else
      insert ('{perim}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 7;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.btnVDimClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{vdim}', s, LastCsr+1);
    end
    else
      insert ('{vdim}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 6;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.btnVolClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{vol}', s, LastCsr+1);
    end
    else
      insert ('{vol}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 5;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.btnVUnitsClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{vunit}', s, LastCsr+1);
    end
    else
      insert ('{vunit}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 8;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.Button1Click(Sender: TObject);
var
  Ini	: TIniFIle;
begin
	Ini := TIniFile.Create (ExtractFilePath(Application.ExeName) + 'SPLayout.ini');
  try
    Ini.WriteString('Rpt', 'Title', EdTitle.Text);
    Ini.WriteString('Rpt', 'SubHead', EdSubHdg.Text);

    Ini.WriteString('Hdgs', '1', edHead1.Text);
    Ini.WriteString('Hdgs', '2', edHead2.Text);
    Ini.WriteString('Hdgs', '3', edHead3.Text);
    Ini.WriteString('Hdgs', '4', edHead4.Text);
    Ini.WriteString('Hdgs', '5', edHead5.Text);
    Ini.WriteString('Hdgs', '6', edHead6.Text);
    Ini.WriteString('Hdgs', '7', edHead7.Text);
    Ini.WriteString('Hdgs', '8', edHead8.Text);
    Ini.WriteString('Hdgs', '9', edHead9.Text);
    Ini.WriteString('Hdgs', '10', edHead10.Text);

    Ini.WriteString('Cols', '1', edData1.Text);
    Ini.WriteString('Cols', '2', edData2.Text);
    Ini.WriteString('Cols', '3', edData3.Text);
    Ini.WriteString('Cols', '4', edData4.Text);
    Ini.WriteString('Cols', '5', edData5.Text);
    Ini.WriteString('Cols', '6', edData6.Text);
    Ini.WriteString('Cols', '7', edData7.Text);
    Ini.WriteString('Cols', '8', edData8.Text);
    Ini.WriteString('Cols', '9', edData9.Text);
    Ini.WriteString('Cols', '10', edData10.Text);

    Ini.WriteString('Tots', '1', edTot1.Text);
    Ini.WriteString('Tots', '2', edTot2.Text);
    Ini.WriteString('Tots', '3', edTot3.Text);
    Ini.WriteString('Tots', '4', edTot4.Text);
    Ini.WriteString('Tots', '5', edTot5.Text);
    Ini.WriteString('Tots', '6', edTot6.Text);
    Ini.WriteString('Tots', '7', edTot7.Text);
    Ini.WriteString('Tots', '8', edTot8.Text);
    Ini.WriteString('Tots', '9', edTot9.Text);
    Ini.WriteString('Tots', '10', edTot10.Text);

    Ini.WriteString('SubTots', '1', edSubTot1.Text);
    Ini.WriteString('SubTots', '2', edSubTot2.Text);
    Ini.WriteString('SubTots', '3', edSubTot3.Text);
    Ini.WriteString('SubTots', '4', edSubTot4.Text);
    Ini.WriteString('SubTots', '5', edSubTot5.Text);
    Ini.WriteString('SubTots', '6', edSubTot6.Text);
    Ini.WriteString('SubTots', '7', edSubTot7.Text);
    Ini.WriteString('SubTots', '8', edSubTot8.Text);
    Ini.WriteString('SubTots', '9', edSubTot9.Text);
    Ini.WriteString('SubTots', '10', edSubTot10.Text);
  finally
    ini.Free;
  end;
  Application.Terminate;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  DeleteFile(ExtractFilePath(Application.ExeName) + 'SPLayout.ini');
  Application.Terminate;
end;

procedure TForm2.btnSDimClick(Sender: TObject);
var
  s : string;
begin
  if LastCsr >= 0 then begin
    s := LastEd.Text;
    if LastEd.SelLength > 0 then begin
      delete (s, LastEd.SelStart+1, LastEd.SelLength);
      insert ('{sdim}', s, LastCsr+1);
    end
    else
      insert ('{sdim}', s, LastCsr+1);
    LastEd.Text := s;
    LastCsr := LastCsr + 6;
    LastEd.SetFocus;
  end;
end;

procedure TForm2.edExit(Sender: TObject);
begin
  LastEd := TEdit(Sender);
  LastCsr := (TEdit(Sender)).SelStart;
end;

procedure TForm2.edHeadEnter(Sender: TObject);
begin
    btnNbr.Enabled := false;
    btnName.Enabled := false;
    btnHDim.Enabled := false;
    btnVDim.Enabled := false;
    btnSDim.Enabled := false;
    btnLDim.Enabled := false;
    btnArea.Enabled := false;
    btnAUnit.Enabled := false;
    btnPAUnit.Enabled := false;
    BtnDUnit.Enabled := false;
    btnPerim.Enabled := false;
    btnPArea.Enabled := false;
    btnVol.Enabled := false;
    btnVUnits.Enabled := false;
    btnPct.Enabled := false;
    btnCat.Enabled := false;
    btnLyr.Enabled := false;
end;

procedure TForm2.edEnter(Sender: TObject);
begin
  if Sender = EdSubHdg then
    edHeadEnter (Sender)
  else begin
    btnNbr.Enabled := true;
    btnName.Enabled := true;
    btnHDim.Enabled := true;
    btnVDim.Enabled := true;
    btnSDim.Enabled := true;
    btnLDim.Enabled := true;
    btnArea.Enabled := true;
    btnAUnit.Enabled := true;
    BtnDUnit.Enabled := true;
    btnPerim.Enabled := true;
    btnPArea.Enabled := true;
    btnPAUnit.Enabled := true;
    btnVol.Enabled := true;
    btnVUnits.Enabled := true;
    btnPct.Enabled := true;
  end;
  btnCat.Enabled := true;
  btnLyr.Enabled := true;
  if Sender = LastEd then begin
    (TEdit(Sender)).SelStart := LastCsr;
    (TEdit(Sender)).SelLength := 0;
  end;
end;

procedure TForm2.FormCreate(Sender: TObject);
var
  str : string;
  Ini	: TIniFIle;

begin
  try
    Msgs := TStringList.Create;
    str := ExtractFilePath(Application.ExeName) + 'SpacePlanner.msg';
    Msgs.LoadFromFile(str);
    btnNbr.Hint := Msgs[14];
    btnName.Hint := Msgs[15];
    btnHDim.Hint := Msgs[16];
    btnVDim.Hint := Msgs[17];
    btnSDim.Hint := Msgs[18];
    btnLDim.Hint := Msgs[19];
    btnPerim.Hint := Msgs[20];
    BtnDUnit.Hint := Msgs[21];
    btnArea.Hint := Msgs[22];
    btnAUnit.Hint := Msgs[23];
    btnPArea.Hint := Msgs[24];
    btnPAUnit.Hint := Msgs[25];
    btnVol.Hint := Msgs[26];
    btnVUnits.Hint := Msgs[27];
    btnCat.Hint := Msgs[147];
    btnLyr.Hint := Msgs[146];

    lblCol1.Caption := Msgs[59] + ' 1';
    lblCol2.Caption := Msgs[59] + ' 2';
    lblCol3.Caption := Msgs[59] + ' 3';
    lblCol4.Caption := Msgs[59] + ' 4';
    lblCol5.Caption := Msgs[59] + ' 5';
    lblCol6.Caption := Msgs[59] + ' 6';
    lblCol7.Caption := Msgs[59] + ' 7';
    lblCol8.Caption := Msgs[59] + ' 8';
    lblCol9.Caption := Msgs[59] + ' 9';
    lblCol10.Caption := Msgs[59] + ' 10 ';
    memInstructions.Text := Msgs[89].Replace('|', #13#10);

    LblTitle.Caption := Msgs[61] + ':';
    LblHeadings.Caption := Msgs[64] + ':';
    LblTots.Caption := Msgs[66] + ':';
    LblData.Caption := Msgs[65] + ':';
    LblSubHeads.Caption := Msgs[60] + ':';
    lblSubTots.Caption := Msgs[150] + ':';
  except
    lblMsg.Caption := 'Error loading message file';
  end;

 	Ini := TIniFile.Create (ExtractFilePath(Application.ExeName) + 'SPLayout.ini');
  try
    EdTitle.Text := Ini.ReadString('Rpt', 'Title', '');
    EdSubHdg.Text := Ini.ReadString('Rpt', 'SubHead', '');

    edHead1.Text := Ini.ReadString('Hdgs', '1', '');
    edHead2.Text := Ini.ReadString('Hdgs', '2', '');
    edHead3.Text := Ini.ReadString('Hdgs', '3', '');
    edHead4.Text := Ini.ReadString('Hdgs', '4', '');
    edHead5.Text := Ini.ReadString('Hdgs', '5', '');
    edHead6.Text := Ini.ReadString('Hdgs', '6', '');
    edHead7.Text := Ini.ReadString('Hdgs', '7', '');
    edHead8.Text := Ini.ReadString('Hdgs', '8', '');
    edHead9.Text := Ini.ReadString('Hdgs', '9', '');
    edHead10.Text := Ini.ReadString('Hdgs', '10', '');

    edData1.Text := Ini.ReadString('Cols', '1', '');
    edData2.Text := Ini.ReadString('Cols', '2', '');
    edData3.Text := Ini.ReadString('Cols', '3', '');
    edData4.Text := Ini.ReadString('Cols', '4', '');
    edData5.Text := Ini.ReadString('Cols', '5', '');
    edData6.Text := Ini.ReadString('Cols', '6', '');
    edData7.Text := Ini.ReadString('Cols', '7', '');
    edData8.Text := Ini.ReadString('Cols', '8', '');
    edData9.Text := Ini.ReadString('Cols', '9', '');
    edData10.Text := Ini.ReadString('Cols', '10', '');

    edTot1.Text := Ini.ReadString('Tots', '1', '');
    edTot2.Text := Ini.ReadString('Tots', '2', '');
    edTot3.Text := Ini.ReadString('Tots', '3', '');
    edTot4.Text := Ini.ReadString('Tots', '4', '');
    edTot5.Text := Ini.ReadString('Tots', '5', '');
    edTot6.Text := Ini.ReadString('Tots', '6', '');
    edTot7.Text := Ini.ReadString('Tots', '7', '');
    edTot8.Text := Ini.ReadString('Tots', '8', '');
    edTot9.Text := Ini.ReadString('Tots', '9', '');
    edTot10.Text := Ini.ReadString('Tots', '10', '');


    edSubTot1.Text := Ini.ReadString('SubTots', '1', '');
    edSubTot2.Text := Ini.ReadString('SubTots', '2', '');
    edSubTot3.Text := Ini.ReadString('SubTots', '3', '');
    edSubTot4.Text := Ini.ReadString('SubTots', '4', '');
    edSubTot5.Text := Ini.ReadString('SubTots', '5', '');
    edSubTot6.Text := Ini.ReadString('SubTots', '6', '');
    edSubTot7.Text := Ini.ReadString('SubTots', '7', '');
    edSubTot8.Text := Ini.ReadString('SubTots', '8', '');
    edSubTot9.Text := Ini.ReadString('SubTots', '9', '');
    edSubTot10.Text := Ini.ReadString('SubTots', '10', '');
  finally
    ini.Free;
  end;
end;

procedure TForm2.FormDestroy(Sender: TObject);
begin
  Msgs.Free;
end;

end.
